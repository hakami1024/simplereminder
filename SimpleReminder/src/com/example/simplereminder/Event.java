package com.example.simplereminder;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Event implements Serializable {
	
	private final long MILLISEC_IN_MIN = 60000L;
	
	protected final String name;
	protected final long firstTime; //����� � �������������
	protected final int id;
	//public int repeatTimes;
	//public long interval;
	
	public Event(){
		name = "";
		firstTime = Calendar.getInstance().getTimeInMillis();
		id = createId();
	}
	
	public Event( String name, int hour, int minute )
	{
		this.name = name;
		firstTime = timeToMillis(hour, minute);
		id = createId();
	}
	
	public Event( String name, long firstTime )
	{
		this.name = name;
		this.firstTime = firstTime;
		id = createId();
	}
	
	public Event( String name, int id )
	{
		this.name = name;
		this.id = id;
		this.firstTime = id*MILLISEC_IN_MIN;
	}
	
	@Override
	public String toString()
	{
		return dateToString() + ", " + name;
	}
	
	private String dateToString()
	{
		return new SimpleDateFormat("dd/MM/yyyy kk:mm").format( new Date(firstTime));
	}
	
	//� �������� id ����� ������������ �����
	private int createId()
	{
		return (int)( firstTime / MILLISEC_IN_MIN );
	}

	private long timeToMillis(int hour, int minute) {
		Calendar cal = Calendar.getInstance();
		int curTime = cal.get(Calendar.HOUR)*60 + cal.get(Calendar.MINUTE);
		int alarmTime = hour*60+minute;
		if( curTime > alarmTime )
		{
			cal.add(Calendar.DATE, 1);
		}
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), hour, minute);
		
		return cal.getTimeInMillis();
	}
	
	@Override
	public boolean equals(Object o) {
	    if(!(o instanceof Event)) return false;
	    Event other = (Event) o;
	    return (id == other.id);
	}
	
}
