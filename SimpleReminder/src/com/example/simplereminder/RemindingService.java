package com.example.simplereminder;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class RemindingService extends Service  {
	
	private static final String TAG = "RemindingService";
	private final String REMINDING_COMMAND = "reminding_command";
	protected static final String UPDATE_INTENT = "com.example.simplereminder.update_intent";
	
	private final String EVENT = "event";
	private final int DO_NOTHING = 100;
	private final int START_REMINDING = 101;
	private final int STOP_REMINDING = 102;
	
	private NotificationManager nm;
	private boolean updateNotification = true;
	private int runningCommands = 0;
	protected ArrayList<Event> eventList = new ArrayList<Event>();
	private EventsBinder binder;
	
	Handler toastHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			Toast.makeText(getApplicationContext(), "�����������: " + ((Event)msg.obj).name, Toast.LENGTH_LONG).show();
		}
	};
	
	@Override 
	public void onCreate(){
		super.onCreate();
		Log.d(TAG, "onCreate");
	}
	
	@SuppressLint("Assert")
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int command = intent.getIntExtra(REMINDING_COMMAND, DO_NOTHING);
		
		if(command == DO_NOTHING)
			return Service.START_REDELIVER_INTENT;
		
		Event event = (Event) intent.getSerializableExtra(EVENT);

		switch(command){
		case START_REMINDING:
			runningCommands++;
			remind(event);
			break;
		case STOP_REMINDING:
			binder.deleteEvent(event);
			stopService(startId);
			break;
		}
		return Service.START_REDELIVER_INTENT;
	};
	
	void remind(final Event event) {
		
		setNotification(event, 0);
		Thread notificationThread = new Thread( new Runnable(){
			@Override
			public void run() {
				try {
					int minutes = 0;

					while(updateNotification)
					{
						setNotification(event, minutes);
						minutes++;
						TimeUnit.MINUTES.sleep(1);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}	
		});
		notificationThread.start();
		
		
		Thread toastThread = new Thread( new Runnable()
		{

			@Override
			public void run() {
				try{
					while(updateNotification)
					{
						toastHandler.sendMessage( Message.obtain(toastHandler, 0, event) );
						TimeUnit.SECONDS.sleep(15);
					}
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			
		});
		toastThread.start();
		
	  }
	
	private void setNotification(Event event, int timeDelta){
		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		
		Intent i = new Intent(this, RemindingService.class);
		i.putExtra(REMINDING_COMMAND, STOP_REMINDING);
		i.putExtra(EVENT, event);
		
		
		RemoteViews view=new RemoteViews(this.getPackageName(), R.layout.notification);
		
		PendingIntent intent = PendingIntent.getService(this, event.id, i, PendingIntent.FLAG_CANCEL_CURRENT);
		view.setOnClickPendingIntent(R.id.notif_ok_button, intent);
		view.setTextViewText(R.id.notif_text, event.name+" (������ �������: "+timeDelta+" ���.)");
		
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
				.setSmallIcon( R.drawable.ic_audio_alarm )
	            .setLargeIcon( BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification) )
	            .setTicker("�����������: "+event.name)
	            .setWhen(System.currentTimeMillis())  
	            .setAutoCancel(true)
	            .setOngoing(true)
	            .setDeleteIntent(intent)
	            .setContentTitle("�����������")
	            .setContent(view);
		nm.notify(event.id, mBuilder.build());
		this.startForeground(event.id, mBuilder.build());
	}
	
	private void stopReminding(Event event){
		updateNotification = false;
		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		nm.cancel(event.id);
	}
	
	private void stopService(int startId) {
		runningCommands--;
		if( runningCommands == 0 )
		{
			stopSelf(startId);
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		PendingIntent updater = intent.getParcelableExtra(UPDATE_INTENT);
		if( binder == null )
			binder = new EventsBinder(updater);
		else binder.alifeActivity = true;
		
		return binder;
	}
	
	@Override 
	public boolean onUnbind(Intent intent)
	{
		binder.alifeActivity = false;
		return true;
	}

	protected class EventsBinder extends Binder
	{
		private boolean alifeActivity;
		private ArrayList<Event> events;
		private PendingIntent screenUpdateIntent;
		
		EventsBinder(PendingIntent i)
		{
			super();
			alifeActivity = true;
			events = eventList; 
			screenUpdateIntent = i;
		}
		
		protected void setAlarm(Activity activity, Event event)
		{
			PendingIntent resultPI = activity.createPendingResult(event.id, activity.getIntent(), PendingIntent.FLAG_ONE_SHOT);
			
			Intent i = new Intent(activity, RemindingService.class);
			i.putExtra( REMINDING_COMMAND, START_REMINDING);
			i.putExtra( EVENT, event );
			
			PendingIntent pi = PendingIntent.getService(activity, event.id, i, PendingIntent.FLAG_CANCEL_CURRENT);
			AlarmManager alarmManager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
			alarmManager.set(AlarmManager.RTC_WAKEUP, event.firstTime, pi);
		}

		protected ArrayList<Event> getEvents() {
			return events;
		}

		protected void addEvent(Event event) {
			int currentEventIndex =  events.indexOf(event);

			if( currentEventIndex == -1 )
				events.add(event);
			else
				try {
					events.set(currentEventIndex, event);
					screenUpdateIntent.send();
				} catch (CanceledException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			//eventList.add(event);
		}
		
		protected void deleteEvent(Event event)
		{
			stopReminding(event);
			events.remove(event);
			
			if( alifeActivity )
				try {
					screenUpdateIntent.send();
				} catch (CanceledException e) {
					e.printStackTrace();
				}
		}
		
		protected void deleteEvent(int eventPos)
		{
			stopReminding(events.get(eventPos));
			events.remove(eventPos);
			if( alifeActivity )
				try {
					screenUpdateIntent.send();
				} catch (CanceledException e) {
					e.printStackTrace();
				}
			//eventList.remove(eventId);
		}

		
	}

}
