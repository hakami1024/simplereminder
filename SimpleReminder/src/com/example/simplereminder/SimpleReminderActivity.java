package com.example.simplereminder;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;

import com.example.simplereminder.RemindingService.EventsBinder;

public class SimpleReminderActivity extends Activity {
	
	protected static final String TAG = "SimpleReminderActivity";
	protected static final String EVENT = "com.example.simplereminder.event";
	ListView listView;

	private ArrayAdapter<Event> adapter;
	Intent service;
	EventsBinder eBinder;
	
	ServiceConnection connection = new ServiceConnection() {

		@Override
	    public void onServiceConnected(ComponentName name, IBinder binder) {
			eBinder = (EventsBinder) binder;
			adapter = new ArrayAdapter<Event>(SimpleReminderActivity.this,
				        android.R.layout.simple_list_item_1, eBinder.getEvents());
			listView.setAdapter(adapter);
	    }

		@Override
	    public void onServiceDisconnected(ComponentName name) {}
	}; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_simple_reminder);
		
		listView = (ListView) findViewById(R.id.events_lv);
		
		registerForContextMenu(listView);
		service = new Intent(this, RemindingService.class);
		startService( service );
		
		PendingIntent pi = createPendingResult(0, getIntent(), PendingIntent.FLAG_ONE_SHOT);
		service.putExtra(RemindingService.UPDATE_INTENT, pi);
		bindService(service, connection,  BIND_ABOVE_CLIENT );
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if( adapter != null )
			adapter.notifyDataSetChanged();
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.events_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int id = item.getItemId();
		switch(id) {
		case R.id.action_add:
			setEventsName();
			return true;
		}
		

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add("�������");
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if( item.getTitle().equals("�������") )
		{
			AdapterContextMenuInfo contextAdapter = (AdapterContextMenuInfo)item.getMenuInfo();
			eBinder.deleteEvent(contextAdapter.position);
			adapter.notifyDataSetChanged();
			return true;
		}
		
		return super.onContextItemSelected(item);
	}

	@Override
	  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    adapter.notifyDataSetChanged();
	}
	private void setEventsName() {
		Log.d(TAG, "addEvent");
		AlertDialog.Builder nameDialog = new AlertDialog.Builder(this);
		final EditText nameET = new EditText(this);
		nameDialog.setView(nameET);
		nameDialog.setTitle("�������� �������:");
		nameDialog.setPositiveButton("�����", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String name = nameET.getText().toString();
				setEventTime( name );
			}
		});
		nameDialog.setNegativeButton("��������", null);
		nameDialog.show();
	}

	public void setEventsName(View v)
	{
		setEventsName();
	}
	
	protected void setEventTime(final String name) {
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		OnTimeSetListener timeSetListener = new OnTimeSetListener() {
			
			@Override
			public void onTimeSet(TimePicker view, int hour, int minute) {
				Event e = new Event( name, hour, minute);
				eBinder.setAlarm(SimpleReminderActivity.this, e);
				eBinder.addEvent(e);
				adapter.notifyDataSetChanged();
			}
		};
		
		TimePickerDialog tpd = new TimePickerDialog(this,timeSetListener , hour, minute, true);
		tpd.show();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		unbindService(connection);
	}
}
